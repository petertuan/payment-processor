class Transaction(object):
    def __init__(self, total_amount, payer, debtors):
        self.total_amount = total_amount
        self.payer = payer
        self.debtors = debtors

    @property
    def amount_owed_per_person(self):
        return self.total_amount / len(self.debtors)
