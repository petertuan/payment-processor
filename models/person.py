class Person(object):
    def __init__(self, name):
        self.name = name
        self.owes = []

    def __str__(self):
        return self.name
