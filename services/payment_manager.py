class PaymentManager(object):
    def __init__(self, transactions):
        self.transactions = transactions
        self.parties = set()

    def set_obligations(self):
        for tx in self.transactions:
            for debtor in tx.debtors:
                debtor.owes.append({'payer': tx.payer, 'amount': tx.amount_owed_per_person})
                self.parties.add(debtor)

    def settle_all_debt(self):
        self._combine_similar_paths()
        self._print_all_settled_payments()


    def _combine_similar_paths(self):
        for debtor in self.parties:
            if not debtor.owes:
                # If debtor does not owe anyone, just return
                return debtor

            for obligation in debtor.owes:
                # Check if the person that debtor owes is in debt to the same person
                self._converge_paths_between_people(debtor, obligation['payer'])

    def _converge_paths_between_people(self, personA, personB):
        # Iterate through people both parties owe
        # If you find the same person, converge the two by removing the lower weight and adding to the higher
        for i in personA.owes:
            for j in personB.owes:
                if i['payer'] == j['payer']:
                    # Converge
                    if i['amount'] >= j['amount']:
                        j['amount'] += i['amount']
                        personA.owes.remove(i)
                    elif i['amount'] > j['amount']:
                        i['amount'] += j['amount']
                        personB.owes.remove(j)

    def _print_all_settled_payments(self):
        for debtor in self.parties:
            for obligation in debtor.owes:
                print('{0} paid {1} ${2}'.format(debtor, obligation['payer'], obligation['amount']))

    @classmethod
    def settle_single_transaction_debt(cls, tx):
        """
        Class method used to settle debts of a single transaction
        (First part of the problem)
        """
        amount_owed = tx.amount_owed_per_person
        for debtor in tx.debtors:
            if debtor != tx.payer:
                print('{0} paid {1} ${2}'.format(debtor, tx.payer, amount_owed))
