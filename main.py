from models.person import Person
from models.transaction import Transaction
from services.payment_manager import PaymentManager


class Main(object):
    def __init__(self):
        # Our nodes
        sarah = Person('Sarah')
        alice = Person('Alice')
        john = Person('John')
        bob = Person('Bob')

        tx1 = Transaction(400, sarah, [alice, john, bob, sarah])
        tx2 = Transaction(100, john, [alice, bob])
        transactions = [tx1, tx2]

        # Set up directed graph with transactions
        payment_manager = PaymentManager(transactions)
        payment_manager.set_obligations()
        payment_manager.settle_all_debt()

        # Can also use the PaymentManager to settle a single transaction
        # PaymentManager.settle_single_transaction_debt(tx1)


if __name__ == '__main__':
    Main()
